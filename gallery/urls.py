from django.conf.urls import url
from gallery import views

urlpatterns = [
    url(r'^$', views.index_page_gallery, name='index_page_gallery'),
    url(r'^(?P<id>\d+)/$', views.filter_page_gallery, name='filter_page_gallery'),
]



