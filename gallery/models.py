from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError


class Album(models.Model):
    title = models.CharField(u'название', max_length=100, blank=True, default='')
    ordering = models.SmallIntegerField(_('Порядковый номер'), )

    class Meta:
        ordering = ('title',)
        verbose_name = _('Альбом')
        verbose_name_plural = _('Перечень альбомов')
        ordering = ('ordering',)

    def __str__(self):
        return self.title


class Video(models.Model):
    album = models.ForeignKey(Album, verbose_name=u'альбом', related_name='videos')
    video_file = models.FileField(u'видео файл', upload_to=u'video/')
    image = models.ImageField(u'изображение', blank=True, upload_to="pic/materials/")
    ordering = models.SmallIntegerField(_('Порядковый номер'), blank=True, )

    def thumb_img(self):
        if self.image:
            return u'<img src="%s" width="100"/>' % self.image.url
        else:
            return '(none)'

    thumb_img.short_description = 'Thumb'
    thumb_img.allow_tags = True

    class Meta:
        verbose_name = _('Видео')
        verbose_name_plural = _('Перечень видео')
        ordering = ('ordering',)

    def __str__(self):
        return ''


class Photo(models.Model):
    album = models.ForeignKey(Album, verbose_name=u'альбом', related_name='photos')
    image = models.ImageField(u'изображение', upload_to="pic/materials/")
    ordering = models.SmallIntegerField(_('Порядковый номер'), )

    def thumb_img(self):
        if self.image:
            return u'<img src="%s" width="100"/>' % self.image.url
        else:
            return '(none)'

    thumb_img.short_description = 'Thumb'
    thumb_img.allow_tags = True

    class Meta:
        verbose_name = _('Фотография')
        verbose_name_plural = _('Перечень фотографий')
        ordering = ('ordering',)

    def __str__(self):
        return ''

class Comments(models.Model):
    date = models.DateTimeField(_('Дата'), auto_now_add=False, blank=True)
    comment = models.TextField(_('Описание'))
    userid = models.PositiveSmallIntegerField()

    class Meta:
        ordering = ('date',)
        verbose_name = _('Комментарии')
        verbose_name_plural = _('Комментарии')

    def __str__(self):
        return self.comment
