from django.contrib import admin
from django.forms import ModelForm
from gallery.models import Photo, Album, Video
from gallery.widgets import MultiFileInput
from django.contrib import messages
from django.shortcuts import redirect
from django.utils.encoding import smart_str


class AlbumAdminForm(ModelForm):
    class Meta:
        model = Album
        fields = ['title', 'ordering']


class AlbumAdmin(admin.ModelAdmin):
    form = AlbumAdminForm
    list_display = [
        'title',
        'ordering'
    ]


class PhotoAdminForm(ModelForm):
    class Meta:
        model = Photo
        widgets = {'image': MultiFileInput}
        fields = ['album', 'image', 'ordering', ]


class PhotoAdmin(admin.ModelAdmin):
    form = PhotoAdminForm
    list_display = [
        'thumb_img',
        'album',
        'ordering',
    ]

    def add_view(self, request, *args, **kwargs):
        images = request.FILES.getlist('image', [])
        is_valid = PhotoAdminForm(request.POST, request.FILES).is_valid()

        if request.method == 'GET' or len(images) <= 1 or not is_valid:
            return super(PhotoAdmin, self).add_view(request, *args, **kwargs)
        for image in images:
            album_id = request.POST['album']
            try:
                photo = Photo(album_id=album_id, image=image)
                photo.save()
            except Exception as e:
                messages.error(request, smart_str(e))

        return redirect('/admin/materials/photo/')


class VideoAdminForm(ModelForm):
    class Meta:
        model = Video
        widgets = {'image': MultiFileInput}
        fields = ['album', 'image', 'video_file', 'ordering']


class VideoAdmin(admin.ModelAdmin):
    form = VideoAdminForm
    list_display = [
        'album',
        'thumb_img',
        'ordering',
    ]

    def add_view(self, request, *args, **kwargs):
        images = request.FILES.getlist('image', [])
        is_valid = PhotoAdminForm(request.POST, request.FILES).is_valid()

        if request.method == 'GET' or len(images) <= 1 or not is_valid:
            return super(VideoAdmin, self).add_view(request, *args, **kwargs)
        for image in images:
            album_id = request.POST['album']
            try:
                photo = Video(album_id=album_id, image=image)
                photo.save()
            except Exception as e:
                messages.error(request, smart_str(e))

        return redirect('/admin/materials/photo/')


admin.site.register(Album, AlbumAdmin)
admin.site.register(Photo, PhotoAdmin)
admin.site.register(Video, VideoAdmin)