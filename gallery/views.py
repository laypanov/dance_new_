from carusel.models import Image
from sponsors.models import Sponsors
from mmenu.models import Menu, MenuEvent
from other.models import Other
from hotel.models import Hotel
from django.shortcuts import render, get_object_or_404
from partners.models import Partners
from gallery.models import Photo
from html_meta.models import MetaInformation
from gallery.models import Album


def index_page_gallery(request):
    arr = {}
    arr["citems"] = Image.objects.all()
    arr["sponsors"] = Sponsors.objects.all()
    arr["menuevent"] = MenuEvent.objects.all()
    arr["mmenu"] = Menu.objects.all()
    arr["other_info"] = Other.objects.first()
    arr["hotel"] = Hotel.objects.first()
    arr["partners"] = Partners.objects.all()
    arr['photo'] = Photo.objects.all()
    arr['albums'] = Album.objects.all()
    arr["meta_information"] = MetaInformation.objects.first()
    arr["gallery_menu"] = Album.objects.all()
    return render(request, 'gallery/index.html', arr)


def filter_page_gallery(request, id):
    arr = {}
    albums = get_object_or_404(Album, id=id)
    arr["citems"] = Image.objects.all()
    arr["sponsors"] = Sponsors.objects.all()
    arr["menuevent"] = MenuEvent.objects.all()
    arr["mmenu"] = Menu.objects.all()
    arr["other_info"] = Other.objects.first()
    arr["hotel"] = Hotel.objects.first()
    arr["partners"] = Partners.objects.all()
    arr['photo'] = Photo.objects.all()
    arr['albums'] = Album.objects.filter(id=id)
    arr["meta_information"] = MetaInformation.objects.first()
    arr["gallery_menu"] = Album.objects.all()
    return render(request, 'gallery/index.html', arr)