from django.shortcuts import redirect


def main(request):
    return redirect('main:landing_page')
