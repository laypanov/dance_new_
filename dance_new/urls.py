from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.views.static import serve
from django.conf.urls.i18n import i18n_patterns
from dance_new import views

admin.autodiscover()

urlpatterns = i18n_patterns(
    url(r'^jet/', include('jet.urls', 'jet')),
    url(r'^jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^news/', include('news.urls', namespace='main')),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^partners/', include('partners.urls')),
    url(r'^sponsors/', include('sponsors.urls')),
    url(r'^city/', include('city.urls')),
    url(r'^event/', include('event.urls')),
    url(r'^hotel/', include('hotel.urls')),
    url(r'^gallery/', include('gallery.urls')),
    url(r'^$', views.main, name='main_page'),
)

if settings.DEBUG:
    urlpatterns += [
        url(r'media/(?P<path>.*)$', serve, {
            'document_root': settings.MEDIA_ROOT,
        }),
    ]
