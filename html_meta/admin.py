from django.contrib import admin
from html_meta.models import MetaInformation


class AdminMetaInformation(admin.ModelAdmin):
    fields = []
    list_display = [field.name for field in MetaInformation._meta.fields if field.name != "id"]


admin.site.register(MetaInformation, AdminMetaInformation)
