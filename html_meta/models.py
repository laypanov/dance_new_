from django.db import models


class MetaInformation(models.Model):
    title = models.CharField(
        verbose_name='Заголовок',
        max_length=160
    )
    description = models.TextField(
        verbose_name='Описание',
        max_length=1000
    )
    keywords = models.TextField(
        verbose_name='Ключевые слова',
        max_length=1000
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Метаданные сайта'
        verbose_name_plural = verbose_name