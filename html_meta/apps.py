from django.apps import AppConfig


class HtmlMetaConfig(AppConfig):
    name = 'html_meta'
    verbose_name = 'Метаданные сайта'
