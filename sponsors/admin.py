from django.contrib import admin
from parler.admin import TranslatableAdmin, TranslatableStackedInline, TranslatableTabularInline
from .models import Sponsors


class SponsorsAdmin(TranslatableAdmin):
    list_display = ('name', 'language_column')
    fieldsets = (
        (None, {
            'fields': ('name', 'description', 'image'),
        }),
    )


admin.site.register(Sponsors, SponsorsAdmin)
