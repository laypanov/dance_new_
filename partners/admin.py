from django.contrib import admin
from parler.admin import TranslatableAdmin, TranslatableStackedInline, TranslatableTabularInline
from .models import Partners


class PartnersAdmin(TranslatableAdmin):
    list_display = ('name', 'language_column')
    fieldsets = (
        (None, {
            'fields': ('name', 'description', 'url', 'number', 'image'),
        }),
    )

    class Media:
        js = ('site/js/jquery.min.js', "site/js/prefileview.js",)

admin.site.register(Partners, PartnersAdmin)
