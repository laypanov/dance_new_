from django.shortcuts import render
#from news.models import News
from django.utils.translation import get_language
from django.views.generic import ListView, DetailView
from news.models import News
from carusel.models import Image
from partners.models import Partners
from mmenu.models import Menu, MenuEvent
from other.models import Other
from partners.models import Partners
from parler.views import TranslatableSlugMixin
from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from datetime import datetime
from html_meta.models import MetaInformation
from gallery.models import Album


def index_page_partners(request):
    arr = {}
    arr["citems"] = Image.objects.all()
    arr["partners"] = Partners.objects.all()
    arr["menuevent"] = MenuEvent.objects.all()
    arr["mmenu"] = Menu.objects.all()
    arr["other_info"] = Other.objects.first()
    arr["meta_information"] = MetaInformation.objects.first()
    arr["gallery_menu"] = Album.objects.all()
    return render(request, 'partners/index.html', arr)
