from __future__ import unicode_literals
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from parler.models import TranslatableModel, TranslatedFields
from ckeditor_uploader.fields import RichTextUploadingField

@python_2_unicode_compatible
class Partners(TranslatableModel):
    translations = TranslatedFields(
        name=models.CharField(
            verbose_name='Название',
            max_length=50,
        ),
        description=RichTextUploadingField(
            verbose_name='Описание',
            blank=True,
        ),
        url=models.CharField(
            verbose_name='URL',
            max_length=200,
            blank=True,
            null=True,
            default='#',
        ),
        image=models.ImageField(
            verbose_name='Изображение',
            upload_to='partners_images/',
            blank=True,
            null=True,
        ),

    )

    class Meta:
        verbose_name = 'Парнеры'
        verbose_name_plural = 'Парнеры'

    def image_img(self):
        if self.image:
            return '<img src="%s" width="250"/>' % self.image.url
        else:
            return '(none)'

    image_img.short_description = 'Thumb'
    image_img.allow_tags = True

    def __int__(self):
        return self.image_img
