from __future__ import unicode_literals
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from parler.models import TranslatableModel, TranslatedFields
from ckeditor_uploader.fields import RichTextUploadingField


@python_2_unicode_compatible
class Image(TranslatableModel):
    translations = TranslatedFields(
        name=RichTextUploadingField(
            verbose_name='Название',
        ),
    )

    class Meta:
        verbose_name = 'Карусель на главной странице'
        verbose_name_plural = 'Карусель на главной странице'

    def __int__(self):
        return self.id
