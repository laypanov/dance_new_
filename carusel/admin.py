from django.contrib import admin
from parler.admin import TranslatableAdmin, TranslatableStackedInline, TranslatableTabularInline
from .models import Image


class ImageAdmin(TranslatableAdmin):
    list_display = ('id', 'language_column')
    fieldsets = (
        (None, {
            'fields': ('name',),
        }),
    )

admin.site.register(Image, ImageAdmin)
