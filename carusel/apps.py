from django.apps import AppConfig


class CaruselConfig(AppConfig):
    name = 'carusel'
    verbose_name = 'Карусель'
