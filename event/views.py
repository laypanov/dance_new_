from carusel.models import Image
from sponsors.models import Sponsors
from mmenu.models import Menu, MenuEvent
from other.models import Other
from event.models import Program, Registration, Juoges, Venue, Final
from partners.models import Partners
from html_meta.models import MetaInformation
from django.shortcuts import render, get_object_or_404
from gallery.models import Album


def index_page_program(request):
    arr = {}
    arr["citems"] = Image.objects.all()
    arr["sponsors"] = Sponsors.objects.all()
    arr["menuevent"] = MenuEvent.objects.all()
    arr["mmenu"] = Menu.objects.all()
    arr["other_info"] = Other.objects.first()
    arr["program"] = Program.objects.first()
    arr["partners"] = Partners.objects.all()
    arr["meta_information"] = MetaInformation.objects.first()
    arr["gallery_menu"] = Album.objects.all()
    return render(request, 'event/program/index.html', arr)


def index_page_registration(request):
    arr = {}
    arr["citems"] = Image.objects.all()
    arr["sponsors"] = Sponsors.objects.all()
    arr["menuevent"] = MenuEvent.objects.all()
    arr["mmenu"] = Menu.objects.all()
    arr["other_info"] = Other.objects.first()
    arr["registration"] = Registration.objects.first()
    arr["partners"] = Partners.objects.all()
    arr["meta_information"] = MetaInformation.objects.first()
    arr["gallery_menu"] = Album.objects.all()
    return render(request, 'event/registration/index.html', arr)


def index_page_juoges(request):
    arr = {}
    arr["citems"] = Image.objects.all()
    arr["sponsors"] = Sponsors.objects.all()
    arr["menuevent"] = MenuEvent.objects.all()
    arr["mmenu"] = Menu.objects.all()
    arr["other_info"] = Other.objects.first()
    arr["juoges"] = Juoges.objects.first()
    arr["partners"] = Partners.objects.all()
    arr["meta_information"] = MetaInformation.objects.first()
    arr["gallery_menu"] = Album.objects.all()
    return render(request, 'event/juoges/index.html', arr)


def index_page_venue(request):
    arr = {}
    arr["citems"] = Image.objects.all()
    arr["sponsors"] = Sponsors.objects.all()
    arr["menuevent"] = MenuEvent.objects.all()
    arr["mmenu"] = Menu.objects.all()
    arr["other_info"] = Other.objects.first()
    arr["venue"] = Venue.objects.first()
    arr["partners"] = Partners.objects.all()
    arr["meta_information"] = MetaInformation.objects.first()
    arr["gallery_menu"] = Album.objects.all()
    return render(request, 'event/venue/index.html', arr)


def index_page_final(request):
    arr = {}
    arr["citems"] = Image.objects.all()
    arr["sponsors"] = Sponsors.objects.all()
    arr["menuevent"] = MenuEvent.objects.all()
    arr["mmenu"] = Menu.objects.all()
    arr["other_info"] = Other.objects.first()
    arr["final"] = Final.objects.first()
    arr["partners"] = Partners.objects.all()
    arr["meta_information"] = MetaInformation.objects.first()
    arr["gallery_menu"] = Album.objects.all()
    return render(request, 'event/final/index.html', arr)
