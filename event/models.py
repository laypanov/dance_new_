from __future__ import unicode_literals
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from parler.models import TranslatableModel, TranslatedFields
from ckeditor_uploader.fields import RichTextUploadingField


@python_2_unicode_compatible
class Program(TranslatableModel):
    translations = TranslatedFields(
        description=RichTextUploadingField(
            verbose_name='Редктор страницы'
        ),
    )

    class Meta:
        verbose_name = 'Программа'
        verbose_name_plural = 'Программа'


class Registration(TranslatableModel):
    translations = TranslatedFields(
        description=RichTextUploadingField(
            verbose_name='Редктор страницы'
        ),
    )

    class Meta:
        verbose_name = 'Регистрация'
        verbose_name_plural = 'Регистрация'


class Juoges(TranslatableModel):
    translations = TranslatedFields(
        description=RichTextUploadingField(
            verbose_name='Редктор страницы'
        ),
    )

    class Meta:
        verbose_name = 'Судьи'
        verbose_name_plural = 'Судьи'


class Venue(TranslatableModel):
    translations = TranslatedFields(
        description=RichTextUploadingField(
            verbose_name='Редктор страницы'
        ),
    )

    class Meta:
        verbose_name = 'Место проведения'
        verbose_name_plural = 'Место проведения'


class Final(TranslatableModel):
    translations = TranslatedFields(
        description=RichTextUploadingField(
            verbose_name='Редктор страницы'
        ),
    )

    class Meta:
        verbose_name = 'Результаты'
        verbose_name_plural = 'Результаты'
