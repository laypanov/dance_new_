from django.contrib import admin
from parler.admin import TranslatableAdmin, TranslatableStackedInline, TranslatableTabularInline
from .models import Program, Registration, Juoges, Venue, Final


class ProgramAdmin(TranslatableAdmin):
    list_display = ('id', 'language_column')
    fieldsets = (
        (None, {
            'fields': ('description',),
        }),
    )

admin.site.register(Program, ProgramAdmin)


class RegistrationAdmin(TranslatableAdmin):
    list_display = ('id', 'language_column')
    fieldsets = (
        (None, {
            'fields': ('description',),
        }),
    )

admin.site.register(Registration, RegistrationAdmin)


class JuogesAdmin(TranslatableAdmin):
    list_display = ('id', 'language_column')
    fieldsets = (
        (None, {
            'fields': ('description',),
        }),
    )

admin.site.register(Juoges, JuogesAdmin)


class VenueAdmin(TranslatableAdmin):
    list_display = ('id', 'language_column')
    fieldsets = (
        (None, {
            'fields': ('description',),
        }),
    )

admin.site.register(Venue, VenueAdmin)


class FinalAdmin(TranslatableAdmin):
    list_display = ('id', 'language_column')
    fieldsets = (
        (None, {
            'fields': ('description',),
        }),
    )

admin.site.register(Final, FinalAdmin)
