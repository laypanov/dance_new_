from django.conf.urls import url
from event import views

urlpatterns = [
    url(r'^$', views.index_page_program, name='index_page_program'),
    url(r'^registration/', views.index_page_registration, name='index_page_registration'),
    url(r'^program/', views.index_page_program, name='index_page_program'),
    url(r'^juoges/', views.index_page_juoges, name='index_page_juoges'),
    url(r'^venue/', views.index_page_venue, name='index_page_venue'),
    url(r'^final/', views.index_page_final, name='index_page_final'),
]



