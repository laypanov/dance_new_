from carusel.models import Image
from sponsors.models import Sponsors
from mmenu.models import Menu, MenuEvent
from other.models import Other
from city.models import City
from partners.models import Partners
from html_meta.models import MetaInformation
from django.shortcuts import render, get_object_or_404
from gallery.models import Album


def index_page_city(request):
    arr = {}
    arr["citems"] = Image.objects.all()
    arr["sponsors"] = Sponsors.objects.all()
    arr["menuevent"] = MenuEvent.objects.all()
    arr["mmenu"] = Menu.objects.all()
    arr["other_info"] = Other.objects.first()
    arr["city"] = City.objects.first()
    arr["partners"] = Partners.objects.all()
    arr["meta_information"] = MetaInformation.objects.first()
    arr["gallery_menu"] = Album.objects.all()
    return render(request, 'city/index.html', arr)
