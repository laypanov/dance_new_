from __future__ import unicode_literals
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from parler.models import TranslatableModel, TranslatedFields
from ckeditor_uploader.fields import RichTextUploadingField


@python_2_unicode_compatible
class City(TranslatableModel):
    translations = TranslatedFields(
        description=RichTextUploadingField(
            verbose_name='Редктор страницы'
        ),
    )

    class Meta:
        verbose_name = 'О ростове'
        verbose_name_plural = 'О ростове'

    def __int__(self):
        return self.image_img
