from django.contrib import admin
from parler.admin import TranslatableAdmin, TranslatableStackedInline, TranslatableTabularInline
from .models import City


class CityAdmin(TranslatableAdmin):
    list_display = ('id', 'language_column')
    fieldsets = (
        (None, {
            'fields': ('description',),
        }),
    )

admin.site.register(City, CityAdmin)
