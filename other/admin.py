from django.contrib import admin
from .models import Other
from django.contrib.admin.widgets import AdminTextInputWidget, AdminTextareaWidget
from parler.admin import TranslatableAdmin, TranslatableStackedInline, TranslatableTabularInline
from parler.forms import TranslatableModelForm, TranslatedField


class OtherAdminForm(TranslatableModelForm):
    title = TranslatedField(widget=AdminTextInputWidget)


class OtherAdmin(TranslatableAdmin):
    form = OtherAdminForm
    list_display = ('title', 'language_column')
    fieldsets = (
        (None, {
            'fields': ('news_adv', 'partners_adv', 's_url_vk', 's_url_fb', 's_url_inst', 'mk_url',),
        }),
        ("Событие на панеле меню(время до)", {
            'fields': ('title', 'date', ),
        })
    )


admin.site.register(Other, OtherAdmin)

