from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from parler.models import TranslatableModel, TranslatedFields
from parler.utils.context import switch_language


@python_2_unicode_compatible
class Other(TranslatableModel):
    translations = TranslatedFields(
        title=models.CharField(
            "Текст",
            max_length=200,
            blank=True
        ),
        date=models.DateTimeField(
            verbose_name=_('Время до события'),
            blank=True
        ),
        s_url_vk=models.CharField(
            verbose_name=_('Ссылка на ВК'),
            max_length=200,
            blank=True,
        ),
        s_url_fb=models.CharField(
            verbose_name=_('Ссылка на ФБ'),
            max_length=200,
            blank=True,
        ),
        s_url_inst=models.CharField(
            verbose_name=_('Ссылка на ИНСТ'),
            max_length=200,
            blank=True,
        ),
        mk_url=models.CharField(
            verbose_name=_('Ссылка на майкл'),
            max_length=200,
            blank=True,
        ),
        news_adv=models.CharField(
            "Название кнопки для подробного просмотра новостей",
            max_length=200,
            blank=True),
        partners_adv=models.CharField(
            "Название заголовка в разделе для парнеров",
            max_length=200,
            blank=True),
    )

    class Meta:
        verbose_name = _('Дополнительная информация')
        verbose_name_plural = _('Дополнительная информация')

    def __str__(self):
        return "{0}".format(self.title)

