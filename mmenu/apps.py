from django.apps import AppConfig


class MmenuConfig(AppConfig):
    name = 'mmenu'
    verbose_name = 'Панель меню'
