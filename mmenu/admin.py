from django.contrib import admin
from django.contrib.admin.widgets import AdminTextInputWidget, AdminTextareaWidget
from parler.admin import TranslatableAdmin, TranslatableStackedInline, TranslatableTabularInline
from .models import Menu, MenuEvent
from parler.forms import TranslatableModelForm, TranslatedField


class MenuAdmin(TranslatableAdmin):
    list_display = ('name', 'language_column', 'ordering')
    fieldsets = (
        (None, {
            'fields': ('name', 'uri', 'ordering',),
        }),
    )


admin.site.register(Menu, MenuAdmin)


class MenuEventAdmin(TranslatableAdmin):
    list_display = ('name', 'language_column', 'ordering')
    fieldsets = (
        (None, {
            'fields': ('name', 'uri', 'ordering',),
        }),
    )


admin.site.register(MenuEvent, MenuEventAdmin)
