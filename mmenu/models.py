from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from parler.models import TranslatableModel, TranslatedFields
from django.core.urlresolvers import reverse


@python_2_unicode_compatible
class MenuEvent(TranslatableModel):
    ordering = models.SmallIntegerField(_('Порядковый номер'), )
    translations = TranslatedFields(
        name=models.CharField(
            max_length=150,
            verbose_name=_('Название'),
        ),
        uri=models.CharField(
            verbose_name=_('URL'),
            help_text=_('Ссылка на страницу'),
            max_length=150,
            blank=True,
            null=True,
        ),
    )

    class Meta:
        verbose_name = _('Подменю турнир')
        verbose_name_plural = _('Подменю турнир')
        ordering = ('ordering',)

    def get_absolute_url(self):
        return "/%s/" % self.uri

    def __unicode__(self):
        return self.name


@python_2_unicode_compatible
class Menu(TranslatableModel):
    ordering = models.SmallIntegerField(_('Порядковый номер'),)
    translations = TranslatedFields(
        name=models.CharField(
            max_length=80,
            verbose_name=_('Название'),
        ),
        uri=models.CharField(
            verbose_name=_('URL'),
            help_text=_('Ссылка на страницу'),
            max_length=150,
            blank=True,
            null=True,
        ),
    )

    class Meta:
        verbose_name = _('Основное меню')
        verbose_name_plural = _('Основное меню')
        ordering = ('ordering',)

    def get_absolute_url(self):
        return "/%s/" % self.uri

    def __unicode__(self):
        return self.name