from django.conf.urls import url
from news import views
from news.views import get_url_news

urlpatterns = [
    url(r'^$', views.landing_page, name='landing_page'),
    url(r'^news/(?P<id>\d+)/$', get_url_news, name='detal'),
]
