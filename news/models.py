from __future__ import unicode_literals
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from parler.models import TranslatableModel, TranslatedFields
from ckeditor_uploader.fields import RichTextUploadingField
from django.core.urlresolvers import reverse

@python_2_unicode_compatible
class News(TranslatableModel):
    ordering = models.SmallIntegerField('Порядковый номер')
    translations = TranslatedFields(
        title=models.CharField(
            verbose_name='Заголовок',
            max_length=50,
        ),
        full_description=RichTextUploadingField(
            verbose_name='Полный текст новости'
        ),
        short_description=RichTextUploadingField(
            verbose_name='Краткий текст новости'
        ),
    )

    class Meta:
        verbose_name = 'Новости'
        verbose_name_plural = 'Новости'
        ordering = ('ordering',)

    def get_absolute_url(self):
        return reverse('main:detal', args=[self.id])

    def image_img(self):
        if self.image:
            return '<img src="%s" width="250"/>' % self.image.url
        else:
            return '(none)'

    image_img.short_description = 'Thumb'
    image_img.allow_tags = True

    def __int__(self):
        return self.image_img
