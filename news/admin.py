from django.contrib import admin
from parler.admin import TranslatableAdmin, TranslatableStackedInline, TranslatableTabularInline
from .models import News


class NewsAdmin(TranslatableAdmin):
    list_display = ('title', 'language_column', 'ordering',)
    fieldsets = (
        (None, {
            'fields': ('title', 'full_description', 'short_description', 'ordering',),
        }),
    )

admin.site.register(News, NewsAdmin)
