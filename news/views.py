from django.shortcuts import render
#from news.models import News
from django.utils.translation import get_language
from django.views.generic import ListView, DetailView
from news.models import News
from carusel.models import Image
from partners.models import Partners
from mmenu.models import Menu, MenuEvent
from other.models import Other
from parler.views import TranslatableSlugMixin
from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from datetime import datetime
from html_meta.models import MetaInformation
from gallery.models import Album


def landing_page(request):
    arr = {}
    arr["citems"] = Image.objects.all()
    arr["partners"] = Partners.objects.all()
    arr["menuevent"] = MenuEvent.objects.all()
    arr["mmenu"] = Menu.objects.all()
    arr["other_info"] = Other.objects.first()
    arr["meta_information"] = MetaInformation.objects.first()
    arr["gallery_menu"] = Album.objects.all()

    news = News.objects.all()
    paginator = Paginator(news, 25)

    page = request.GET.get('page')
    try:
        arr["news"] = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        arr["news"] = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        arr["news"] = paginator.page(paginator.num_pages)

    return render(request, 'news/index.html', arr)


def get_url_news(request, id):
    arr = {}
    news = get_object_or_404(News, id=id)
    arr["news"] = news
    arr["citems"] = Image.objects.all()
    arr["partners"] = Partners.objects.all()
    arr["menuevent"] = MenuEvent.objects.all()
    arr["mmenu"] = Menu.objects.all()
    arr["other_info"] = Other.objects.first()
    arr["meta_information"] = MetaInformation.objects.first()
    arr["gallery_menu"] = Album.objects.all()
    return render(request, 'news/adv_view.html', arr)
